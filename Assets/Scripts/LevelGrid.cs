﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGrid : MonoBehaviour
{
    public int width;
    public int height;

    public Transform baseObject;
    public Transform Camera;
    
    Grid grid;

    // Start is called before the first frame update
    void Start()
    {
        grid = transform.GetComponent<Grid>();
        CreateGrid();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateGrid()
    {
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j) 
            {
                var newTile = GameObject.Instantiate(baseObject, grid.CellToLocal(new Vector3Int(i, j, 0)), Quaternion.identity, grid.transform);
                newTile.transform.position -= new Vector3(0, 0, -(i + j));
            }
        }
        Destroy(baseObject.gameObject);
        Camera.position = grid.CellToLocal(new Vector3Int((int) (width - 1) / 2,(int) (height - 1) / 2, 0)) + new Vector3Int(0, 0, -15);
    }
}
